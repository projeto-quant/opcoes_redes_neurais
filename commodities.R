library(Quandl)
library(tidyverse)
library(ggplot2)

Quandl.api_key('bub4y4sr_sZm3CBCe75z')

options <- Quandl('OWF/NYM_CL_CL_N2019_IVS')

commodities <- Quandl(c("CEPEA/SOYBEAN.1", "CEPEA/COFFEE_A.1", "CEPEA/CORN.1", "CEPEA/WHEAT_P.1"), start_date="2005-01-01", end_date="2019-01-29")
colnames(commodities) <- c("data","soja", "cafe_arabica", "milho", "aveia")

retornos_log <- function(df) {
  
  idx <- 2:nrow(df)
  
  # Apply over each variable
  lapply_func <- function(x) {

    # Apply over each individual
    vapply(idx, function(y) {
      x[y] <- log(x[y] / x[y-1])
      x[y]
    }, FUN.VALUE = numeric(length = 1L))
  }

  w <- lapply(df[-1], lapply_func)

  w <- as.data.frame(w)
  
  w <- cbind(df[-1,1], w)
  
  colnames(w) <- colnames(df)

  return(w)
}

commodities_log <- retornos_log(commodities)



