# Vetor das variáveis explicativas do modelo
X <- matrix(c(
  0,1,1,
  0,0,1,
  1,1,1,
  1,0,1
),
ncol = 3,
byrow = TRUE
)

# Vetor da variável dependente do modelo
y <- c(1, 0, 0, 1)

X <- cbind(X, y)


#Geramos valores aleatórios entre 0 e 1, que serão usados como os pesos iniciais para a layer 1
vetor_pesos <- runif(ncol(X) * nrow(X))

#Transformando em uma matriz
vetor_pesos <- matrix(
  vetor_pesos,
  nrow = ncol(X),
  ncol = nrow(X),
  byrow = TRUE
)

#Uma lista com os vetores de pesos, variáveis explicativas e da variável dependente
rede_neural <- list(
  # Variáveis explicativas
  input = X,
  # Pesos para a layer 1
  pesos1 = vetor_pesos,
  # Pesos para a layer 2
  pesos2 = matrix(runif(4), ncol = 1),
  # Variável dependente do modelo
  y = y,
  # Matriz onde serão colocados os outputs do modelo
  output = matrix(
    rep(0, times = 4),
    ncol = 1
  )
)
#Função de ativação
#Aqui, serão colocados o vetor de variáveis explicativas, para gerar a primeira layer: a hidden layer
sigmoide <- function(x) {
  1.0 / (1.0 + exp(-x))
}

#Derivada da função sigmoidal, que será utilizada na etapa de backpropagation
derivada_sigmoide <- function(x) {
  x * (1.0 - x)
}
# Função perda
# É a soma dos erros quadrados da variável dependente pelo output gerado pelo moelo

funcao_perda <- function(nn) {
  sum((nn$y - nn$output) ^ 2)
}

# Feedforward

feedforward <- function(nn) {
  
  nn$layer1 <- sigmoide(nn$input %*% nn$pesos1)
  nn$output <- sigmoide(nn$layer1 %*% nn$pesos2)
  
  nn
}

# Backpropagation

backpropagation <- function(nn) {
  
  #Aplicação da regra da cadeia para obter as derivadas parciais da função perda para os pesos1 e pesos 2
  
  d_pesos2 <- (
    t(nn$layer1) %*%
      (2 * (nn$y - nn$output) *
         derivada_sigmoide(nn$output))
  )
  
  d_pesos1 <- (2 * (nn$y - nn$output) * derivada_sigmoide(nn$output)) %*% 
    t(nn$pesos2)
  d_pesos1 <- d_pesos1 * derivada_sigmoide(nn$layer1)
  d_pesos1 <- t(nn$input) %*% d_pesos1
  
  # Atualizando os pesos com as derivadas parciais encontradas na função perda
  nn$pesos1 <- nn$pesos1 + d_pesos1
  nn$pesos2 <- nn$pesos2 + d_pesos2
  
  nn
}

#Treinando o modelo
#Agora, faremos o processo de feedforward e backpropagation iterativamente até alcançar o mínimo da nossa função perda

n <- 1000

df_perda <- data.frame(
  iteracao = 1:n,
  erro = vector("numeric", length = n)
)

for (i in seq_len(1000)) {
  rede_neural <- feedforward(rede_neural)
  rede_neural <- backpropagation(rede_neural)
  
  #Guardando o resultado da função perda
  df_perda$erro[i] <- funcao_perda(rede_neural)
}

# Resultado da nossa rede neural
data.frame(
  "Predito" = round(rede_neural$output, 3),
  "Atual" = y
)
##   Predicted Actual
## 1     0.017      0
## 2     0.975      1
## 3     0.982      1
## 4     0.024      0